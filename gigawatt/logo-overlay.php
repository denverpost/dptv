<style type="text/css">
#logo-overlay
{
    height: 100px!important;
    width: 300px!important;
    background: url('<?php bloginfo('template_directory'); ?>/images/logo-dptv-white.png') no-repeat;
    background-size: 100% 100%;
    float: right;
    bottom: 240px;
    z-index: 15000;
    opacity: 0.4;
}
/*  We've got two sizes of players we iframe in: 480px, used on homepages and section fronts,
    and 654px, used in articles.
*/
@media only screen and (max-width: 654px)
{
    #logo-overlay
    {
        height: 50px!important;
        width: 150px!important;
        bottom: 54px;
    }
}
</style>
<script>
window.onload = function()
{
    var the_player = document.getElementById('playerContainer')
    if ( the_player != null )
    {
        the_player.innerHTML += '<div id="logo-overlay"></div>';
    }
}
</script>
