<?php
class dptv_home_item_widget extends WP_Widget {
	/** constructor */
	function dptv_home_item_widget() {
		parent::WP_Widget(false, $name = "(DPTV) - Home Single-Cat", array("description" => "Home Page Single-Cat Widget"));	
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance) {	
		// Turn $instance array into variables
		if (class_exists( 'Woocommerce' )) {
			global $woocommerce;
		}	
		$instance_defaults = array ('title' => '', 'title_link' => '', 'post_count' => '', 'show_images' => true, 'post_thumb' => true, 'show_dates' => false, 'show_excerpts' => false);
		$instance_args = wp_parse_args( $instance, $instance_defaults );
		extract( $instance_args, EXTR_SKIP );


		// Setup the post filter if it's defined
		if(isset($postfilter) && isset($instance[$postfilter]))
			$filterval = esc_attr($instance[$postfilter]);
		else
			$filterval = 0;

		// Set the base query args
		$args = array(
			"post_type" => $posttype,
			"posts_per_page" => $post_count
		);

		// Filter by the chosen taxonomy
		if(isset($postfilter) && $postfilter != "" && $filterval != "0") :
            if ( $postfilter == 'category_slugs' ):
                $postfilter = 'category';
                if ( strpos($filterval, ',') > 0 ) { $filterval = explode(',', $filterval); }
            endif;
                $args['tax_query'] = array(
                        array(
                            "taxonomy" => $postfilter,
                            "field" => "slug",
                            "terms" => $filterval
                        )
                    );
            $category = get_category_by_slug($filterval);
		endif;

		// Main Post Query
        $layout_columns = 'three';
		$get_posts = new WP_Query($args);  $i = 0; ?> 
		
		
                <li class="column">
                <h2><a href="<?php echo get_category_link($category->term_id); ?>"><?php echo $category->name; ?></a></h2>
			<?php while ( $get_posts->have_posts() ) : $get_posts->the_post(); 

                    $i++;
					if($layout_columns == '4') :
						$width = 220;
						$height = 125;
						$resizer = '220x125';
					elseif($layout_columns == '3') :
						$width = 300;
						$height = 180;
						$resizer = '300x180';
					else:
						$width = 460;
						$height = 259;
						$resizer = '460x259';
					endif;

				global $post;
					$link = get_permalink($post->ID);
                    // *** Added a couple args that get passed to the get_obox_media function that's in gigawatt/ocmx/front-end/media.php
					$image_args  = array('postid' => $post->ID, 'width' => $width, 'height' => $height, 'hide_href' => false, 'exclude_video' => $post_thumb, 'imglink' => false, 'resizer' => $resizer, 'dptv' => true, 'video_id' => get_post_meta($post->ID, 'video_id'));
					$image = get_obox_media($image_args);
                    // Only show the image on the first post in a column.
                    $image = get_obox_media($image_args); 
                    if ( $i > 1 ) $image = '';
?>
					<?php if($image !="") : ?>
					<div class="post-image fitvid">
						<?php echo $image; ?>
					</div>
					<?php endif; ?>
                    <div class="copy">
					<?php if($show_dates != false) : ?><h5 class="date"><?php echo date('d M Y', strtotime($post->post_date)); ?></h5><?php endif; ?>
						<h2 class="post-title" id="headline"><a href="<?php echo $link; ?>"><?php the_title(); ?></a></h2>
				   
					<?php if($show_excerpts != false) :
						if($post->post_excerpt != "") :
							echo "<p>".substr(strip_tags($post->post_excerpt), 0, $maxlen)."...</p>";
						else :
							the_content("");
						endif;
					endif; ?>
                    </div>
			<?php endwhile; ?>
				</li>				
<?php
	}

	/** @see WP_Widget::update */
	function update($new_instance, $old_instance) {
		return $new_instance;
	}

	/** @see WP_Widget::form */
	function form($instance) {
		$instance_defaults = array ('title' => '', 'title_link' => '', 'post_thumb' => 1, 'post_count' => '', 'posttype' => 'post', 'postfilter' => '0');
		$instance_args = wp_parse_args( $instance, $instance_defaults );
		extract( $instance_args, EXTR_SKIP );
		// Setup the post filter if it's defined
		if(isset($postfilter) && isset($instance[$postfilter]))
			$filterval = esc_attr($instance[$postfilter]);

		$post_type_args = array("public" => true, "exclude_from_search" => false, "show_ui" => true);
		$post_types = get_post_types( $post_type_args, "objects");

	 ?>
	<p><em><?php _e("Click Save after selecting a filter from each menu to load the next filter", "ocmx"); ?></em></p>

	<p>
		<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e("Title", "ocmx"); ?><input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php if(isset($title)) echo $title; ?>" /></label>
	</p>
	<p>
		<label for="<?php echo $this->get_field_id('title_link'); ?>"><?php _e('Custom Title Link', 'ocmx'); ?><input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title_link'); ?>" type="text" value="<?php if(isset($title_link)) {echo $title_link;} ?>" /></label>
	</p>
	   
	   <p>
		<label for="<?php echo $this->get_field_id('posttype'); ?>"><?php _e("Display", "ocmx"); ?></label>
		<select size="1" class="widefat" id="<?php echo $this->get_field_id("posttype"); ?>" name="<?php echo $this->get_field_name("posttype"); ?>">
			<option <?php if(!isset($posttype) || $posttype == ""){echo "selected=\"selected\"";} ?> value="">--- Select a Content Type ---</option>
			<?php foreach($post_types as $post_type => $details) : ?>
				<option <?php if(isset($posttype) && $posttype == $post_type){echo "selected=\"selected\"";} ?> value="<?php echo $post_type; ?>"><?php echo $details->labels->name; ?></option>
			<?php endforeach; ?>
		</select>
		</p>

	<?php if($posttype != "") :
		if($posttype != "page") :
			$taxonomyargs = array('post_type' => $posttype, "public" => true, "exclude_from_search" => false, "show_ui" => true);
			$taxonomies = get_object_taxonomies($taxonomyargs,'objects');
			if(is_array($taxonomies) && !empty($taxonomies)) : ?>
				<p>
					<label for="<?php echo $this->get_field_id('postfilter'); ?>"><?php _e("Filter by", "ocmx"); ?></label>
					<select size="1" class="widefat" id="<?php echo $this->get_field_id("postfilter"); ?>" name="<?php echo $this->get_field_name("postfilter"); ?>">
						<option <?php if($postfilter == ""){echo "selected=\"selected\"";} ?> value="">--- Select a Filter ---</option>
						<?php foreach($taxonomies as $taxonomy => $details) : ?>
							<option <?php if($postfilter == $taxonomy){echo "selected=\"selected\"";} ?> value="<?php echo $taxonomy; ?>"><?php echo $details->labels->name; ?></option>
						<?php $validtaxes[] = $taxonomy;
						endforeach; ?>
                            <option <?php if($postfilter == 'category_slugs'){echo "selected=\"selected\"";} ?> value="category_slugs">Category Slugs</option>
                        <?php $validtaxes[] = 'category_slugs'; ?>
					</select>
				</p>
			<?php endif; // !empty($taxonomies)

			if(isset($validtaxes) && $postfilter != "" && ( (is_array($validtaxes) && in_array($postfilter, $validtaxes)) || !is_array($validtaxes) ) ) :
				$tax = get_taxonomy($postfilter);
				$terms = get_terms($postfilter, "orderby=count&hide_empty=0"); ?>
				<p><label for="<?php echo $this->get_field_id($postfilter); ?>"><?php echo $tax->labels->name; ?></label>
                    <?php if ( $postfilter == 'category_slugs' ): ?>
                    <input class="widefat" id="<?php echo $this->get_field_id($postfilter); ?>" name="<?php echo $this->get_field_name($postfilter); ?>" type="text" value="<?php if(isset($category_slugs)) {echo $category_slugs;} ?>" />
                    <?php else: ?>
				   <select size="1" class="widefat" id="<?php echo $this->get_field_id($postfilter); ?>" name="<?php echo $this->get_field_name($postfilter); ?>">
						<option <?php if(isset($filterval) && $filterval == 0){echo "selected=\"selected\"";} ?> value="0">All</option>
						<?php foreach($terms as $term => $details) :?>
							<option  <?php if(isset($filterval) && $filterval == $details->slug){echo "selected=\"selected\"";} ?> value="<?php echo $details->slug; ?>"><?php echo $details->name; ?></option>
						<?php endforeach;?>
					</select>
                    <?php endif; ?>
				</p>
			<?php endif; // isset($postfilter) && $postfilter != ""
		 endif;  // $posttype != "page"
	endif;  // $posttype != "" ?>
	<p>
		<label for="<?php echo $this->get_field_id('post_count'); ?>"><?php _e("Post Count", "ocmx"); ?></label>
		<select size="1" class="widefat" id="<?php echo $this->get_field_id('post_count'); ?>" name="<?php echo $this->get_field_name('post_count'); ?>">
			<?php $i = 1;
			while($i < 13) :?>
				<option <?php if($post_count == $i) : ?>selected="selected"<?php endif; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
			<?php if($i < 1) :
					$i++;
				else:
					$i=($i+1);
				endif;
			endwhile; ?>
		</select>
	</p>
		   <?php if($posttype != "") : ?>
				<p>
					<label for="<?php echo $this->get_field_id('post_thumb'); ?>"><?php _e("Show Images or Videos?", "ocmx"); ?></label>
					<select size="1" class="widefat" id="<?php echo $this->get_field_id('post_thumb'); ?>" name="<?php echo $this->get_field_name('post_thumb'); ?>">
							<option <?php if(isset($post_thumb) && $post_thumb == "none") : ?>selected="selected"<?php endif; ?> value="none"><?php _e("None", "ocmx"); ?></option>
							<option <?php if(isset($post_thumb) && $post_thumb == "1") : ?>selected="selected"<?php endif; ?> value="1"><?php _e("Featured Thumbnails", "ocmx"); ?></option>
							<option <?php if(isset($post_thumb) && $post_thumb == "0") : ?>selected="selected"<?php endif; ?> value="0"><?php _e("Videos", "ocmx"); ?></option>
					</select>
				</p>
				<?php if($posttype != "product") : ?>
				<p>
					<label for="<?php echo $this->get_field_id('show_dates'); ?>">
						<input type="checkbox" <?php if(isset($show_dates) && $show_dates == "on") : ?>checked="checked"<?php endif; ?> id="<?php echo $this->get_field_id('show_dates'); ?>" name="<?php echo $this->get_field_name('show_dates'); ?>">
						<?php _e("Show Dates", "ocmx"); ?>
					</label>
				</p>
				<?php endif; ?>
				<p>
					<label for="<?php echo $this->get_field_id('show_excerpts'); ?>">
						<input type="checkbox" <?php if(isset($show_excerpts) && $show_excerpts == "on") : ?>checked="checked"<?php endif; ?> id="<?php echo $this->get_field_id('show_excerpts'); ?>" name="<?php echo $this->get_field_name('show_excerpts'); ?>">
						Show Excerpts
					</label>
				</p>
			<?php endif;
		
	} // form

}// class

add_action('widgets_init', create_function('', 'return register_widget("dptv_home_item_widget");'));

?>
