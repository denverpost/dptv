// Pause playing video on escape key.
document.onkeydown = function(evt)
{
    evt = evt || window.event;
    if (evt.keyCode == 27) 
    {
        if ( typeof player !== 'undefined' )
        {
            player.pause();
        }
    }
};

window.mobileAndTabletcheck = function() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
}

var w = 0;
var video_count = 0;

// IE
if ( !window.innerWidth )
{
    if ( !(document.documentElement.clientWidth == 0) )
    {
        // strict mode
        w = document.documentElement.clientWidth;
    }
    else
    {
        //quirks mode
        w = document.body.clientWidth;
    }
}
else
{
    // w3c
    w = window.innerWidth;
}

if ( typeof player_ratio === 'undefined' ) var player_ratio = 0.7;
if ( typeof player_dimensions === 'undefined' ) var player_dimensions = { height: 730, width: 1000 };

// *** NEED TO ACCOUNT FOR CHANGES IN WIDTH if user rotates screen from portrait to landscape
if ( w < 1000 )
{
    if ( w < 480 )
    {
        player_ratio = 1;
    }
    w -= 20;
    player_dimensions.height = Math.round(w * player_ratio);
    player_dimensions.width = w;
}
var player;
var config = {
        playlistsPlugin: {"data":["5ec0b4f104fc4cfebd63380310dfc1f5", "6eb0563f2cc94b1094d16d036f80f703","a8e521272fcc49a680fc3fb22b8a35e9","d9b6afb63faa4b18be72463d516b5fbe","f270f61069184511acaf6cff52e8b10a", "4aa8fd4e4b7c425795dd8663094e41db"]},
        autoplay: false,
        loop: false,
        height: player_dimensions.height,
        width: player_dimensions.width,
        "google-ima-ads-manager": { 
            showInAdControlBar: true
        },
        // ***Chartbeat
        onCreate: function(player) {
            var _cbv = window._cbv || ( window._cbv = []);
            _cbv.push(player);
        }
};
// Disable the bottom row of videos if we're not on the homepage.
// We can tell if we're on the homepage because there will be only three /'s in the referer.
if ( window.location.href.split('/').length > 4 && window.location.href.indexOf('iframe') === -1 )
{
    delete config.playlistsPlugin;
    //config.playlistsPlugin = {}
    config.height = 563;
}

// Disable playlists on handhelds and iframe
if ( window.screen.availWidth < 769 || window.innerWidth < 769 )
{
    delete config.playlistsPlugin;

    // Only adjust height if we're not iframing it:
    if ( document.location.href.indexOf('iframe') < 0 )
    {
        config.height = 300;
    }
}

function load_video(embed_code)
{
    // Put a new video in the player.
    // Only play if we're not on android.
    player.setEmbedCode(embed_code);
    if ( mobileAndTabletcheck() === false )
    {
        player.play();
    }
    window.video_count += 1;
}

// Check if the video player starts or stops.
// If it stops after running through an entire video, play the next video
// DO RUN THIS STUFF ON THE HOMEPAGE PLAYER.
if ( typeof the_cats !== 'undefined' || window.document.location.href.indexOf('?p=') > 0 )
{
    // Download the list of videos in this video's category.
    // We take the first category passed. When posts start getting more than one category
    // we will need to revisit this.
    var js_url = '/?cat=' + the_cats[0] + '&js=1';
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = js_url;
    document.getElementsByTagName("head")[0].appendChild(script);

    // The list of videos is in an object called video_list.
    // We look up where the current video is in that list, then take the video after.
    window.setTimeout("var next_video = next(player.embedCode);", 10000);
    var is_playing = 'ready';
    var is_queued = ''; // Create a var to keep track of whether a video is queued.

    // Get the next item from the video list, which is a window variable
    var next = function(video_id) 
    {
        var list_length = video_list.length;
        for (var i = 0; i < list_length; i++) 
        {
            if (video_list[i].video_id === video_id) 
            {
                if ( i + 1 >= list_length ) { return video_list[0]; }
                return video_list[i + 1];
            }
        }
        // If we can't find the place of that video in the list, return the first video
        return video_list[0];
    };

    function remove_node(element)
    {
        var node = document.getElementById(element);
        if (node.parentNode) 
        {
            node.parentNode.removeChild(node);
        }
    }

    // Handle the extra video functionality with this manager object
    var video_manager = {
        init: function ()
        {
            if ( document.location.hash === '#dev' ) this.is_in_dev = '#dev';
            this.get_video_list();

            // The list of videos is in an object called video_list.
            // We look up where the current video is in that list, then take the video after.
            window.setTimeout("video_manager.next_video = video_manager.get_next(player.embedCode);", 1000);
        },
        is_playing: 'ready',
        is_queued: '',
        is_in_dev: '',
        get_video_list: function ()
        {
            // Download the list of videos in this video's category.
            // We take the first category passed. When posts start getting more than one category
            // we will need to revisit this.
            var js_url = '/?cat=' + the_cats[0] + '&js=1';
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = js_url;
            document.getElementsByTagName("head")[0].appendChild(script);
        },
        set_volume: function ()
        {
            // Set the volume on the video
            window.player.setVolume(0);
        },
        remove_node: function remove_node(element)
        {
            var node = document.getElementById(element);
            if (node.parentNode) 
            {
              node.parentNode.removeChild(node);
            }
        },
        get_next: function (video_id) 
        {
            // Get the next item from the video list
            var list_length = video_list.length;
            for (var i = 0; i < list_length; i++) 
            {
                if (video_list[i].video_id === video_id) 
                {
                    if ( i + 1 >= list_length ) { return video_list[0]; }
                    return video_list[i + 1];
                }
            }
            // If we can't find the place of that video in the list, return the first video
            return video_list[0];
        },
        get_new_social: function (new_social)
        {
            // Return a string of markup that's suitable for generating new social-share button tags.
            // This works for the buttons in the player.
            // It expects input that looks something like this:
            //            var new_social = {
            //                escaped_url: encodeURIComponent(next_video.url.replace(/(\r\n|\n|\r)/gm,"")),
            //                escaped_title: encodeURI(next_video.title.replace(/ /gm, '+').replace(/(\r\n|\n|\r)/gm,""))
            //            }
            var new_social_markup = '<ul class="post-meta-social-links" id="sticky-inner">\n\
<li class="post-meta-social pm-twitter">\n\
<a href="javascript:void(0)" onclick="javascript:window.open(\'http://twitter.com/share?text=' + new_social.escaped_title + '&url=' + new_social.escaped_url + '&via=DPTV&related=MollyHughes\', \'twitwin\', \'left=20,top=20,width=500,height=500,toolbar=1,resizable=1\');">\n\
<img src="http://dptv.denverpost.com/wp-content/themes/gigawatt/images/dptv-twitter.png" alt="Tweet this story" /></a></li>\n\
<li class="post-meta-social pm-facebook">\n\
<a href="javascript:void(0)" \n\
onclick="javascript:window.open(\'http://www.facebook.com/sharer/sharer.php?s=100&p[url]=' + new_social.escaped_url + '&p[title]=' + new_social.escaped_title + '&p[summary]=' + new_social.escaped_title + '\', \'fbwin\', \'left=20,top=20,width=500,height=500,toolbar=1,resizable=1\');">\n\
<img src="http://dptv.denverpost.com/wp-content/themes/gigawatt/images/dptv-facebook.png" alt="Facebook" /></a></li><li class="post-meta-social pm-googleplus"><a href="javascript:void(0)" onclick="javascript:window.open(\'http://plus.google.com/share?url=' + new_social.escaped_url + '\', \'gpluswin\', \'left=20,top=20,width=500,height=500,toolbar=1,resizable=1\');">\n\
<img src="http://dptv.denverpost.com/wp-content/themes/gigawatt/images/dptv-google.png" alt="Share on Google Plus" /></a></li>\n\
<div class="clear"></div></ul>';
            return new_social_markup;
        }
    };


    // Check every X seconds to see if the current video has finished playing.
    window.setInterval(function() {
        var in_dev = '';
        if ( document.location.hash === '#dev' ) in_dev = '#dev';

        // Possible player states: loading, playing, paused, ready
        // THIS LOGIC CONTROLS IF THE NEXT VIDEO GETS TRIGGERED.
        if ( player.state !== 'loading' && player.state !== 'buffering' && ( player.state !== is_playing || Math.floor(player.playheadTime) === Math.floor(player.duration) ) )
        {
            is_playing = player.state;
            // THIS LOGIC ALSO CONTROLS IF THE NEXT VIDEO GETS TRIGGERED.
            if ( 
                window.video_count > 0 && is_playing === 'ready' && typeof player.duration !== 'undefined' || 
                Math.floor(player.playheadTime) === Math.floor(player.duration) && typeof player.duration !== 'undefined' && player.duration > 0 && window.video_count > 0 )
            {
                // The player is in ready state after it has finished playing a video.
                // Let's:
                //  1. Download the JSON list of videos in this category.
                //  2. Load the next video.
                //  3. We also need to update the URL and headline.
                //  4. And the social media links.
                // Also, hide the social share buttons, since those are for the previous video.
                if ( in_dev !== '' ) console.log("is_playing: ", is_playing, "\nplayer.playheadTime & player.duration: ", player.playheadTime, player.duration);

                if ( typeof next_video !== 'undefined' )
                {
                    if ( in_dev !== '' ) console.log("is_playing: ", is_playing, "\nplayer.playheadTime & player.duration: ", player.playheadTime, player.duration, "\nnext_video: ", next_video);
                    load_video(next_video.video_id);
                    player.embedCode = next_video.video_id;
                    window.video_count += 1;
            

                    var new_social = {
                        escaped_url: encodeURIComponent(next_video.url.replace(/(\r\n|\n|\r)/gm,"")),
                        escaped_title: encodeURI(next_video.title.replace(/ /gm, '+').replace(/(\r\n|\n|\r)/gm,""))
                    }
                    var new_social_markup = window.video_manager.get_new_social(new_social);

                    // We edit certain elements depending on whether we're on an iframe'd player
                    // or a full-template post player.
                    if ( document.getElementById('headline-link') === null )
                    {
                        // We're on a full-template ordinary post player
                        document.title = next_video.title;
                        document.getElementById('headline').innerHTML = next_video.title;
                        window.history.pushState('', next_video.title, next_video.url + in_dev);
                        document.getElementById('social-player').innerHTML = new_social_markup;
                        if ( document.getElementById('social-tools') !== null ) { remove_node('social-tools'); }
                        if ( document.getElementById('blog-content') !== null ) { remove_node('blog-content'); }
                        if ( document.getElementById('embed-wrapper') !== null ) { remove_node('embed-wrapper'); }
                    }
                    else
                    {
                        // We're in the iframe player
                        document.getElementById('headline-link').innerHTML = next_video.title;
                        document.getElementById('headline-link').href = next_video.url;
                        document.getElementById('social-overlay').innerHTML = new_social_markup;
                    }
                    
                    player.play();
                    window.next_video = next(player.embedCode);
                }
            }
        }

    }, 5000);
}
var _sf_startpt=(new Date()).getTime()
var _sf_async_config={};

function loadChartbeat() {
    window._sf_endpt=(new Date()).getTime();
    var e = document.createElement('script');
    e.setAttribute('language', 'javascript');
    e.setAttribute('type', 'text/javascript');
    e.setAttribute('src', (('https:' == document.location.protocol) ? 'https://a248.e.akamai.net/chartbeat.download.akamai.com/102508/' : 'http://static.chartbeat.com/') + 'js/chartbeat_video.js');
    document.body.appendChild(e);
}

function executeLoad() {
    var chbpathArray = window.location.pathname.split( '/' );
    if (chbpathArray[1] == '/' || (chbpathArray[1] == '' && chbpathArray[0] == '') || chbpathArray[1].substring(0,3) == 'ci_') {
        var chbsecondLevelLocation = 'homepage';
    } else {
        var chbsecondLevelLocation = chbpathArray[1]; 
    }
    _sf_async_config.uid = 2671;
    _sf_async_config.domain = 'denverpost.com';
    _sf_async_config.sections = 'video';
    //_sf_async_config.path = window.location.pathname;
    _sf_async_config.videoType = 'ooyala';
    _sf_async_config.useCanonical = true;
    _sf_async_config.autoDetect = false;
    //console.log('>>>>> Chartbeat loading with secName: "' + secName + ',"" s.channel: "' + s.channel + '," and secondLevelLocation: "' + secondLevelLocation + '."');
    loadChartbeat();
}
