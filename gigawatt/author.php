<?php get_header(); ?>

<ul class="double-cloumn clearfix">
    <li id="left-column">	
        <ul class="blog-main-post-container clearfix">
			<?php if (have_posts()) :
                while (have_posts()) :	the_post(); setup_postdata($post);
                    get_template_part("/functions/fetch-list");
                endwhile;
            else :
                ocmx_no_posts();
            endif; ?>
        </ul>
		<?php motionpic_pagination("clearfix", "pagination clearfix"); ?>
	</li>
    <li id="right-column">
        <ul class="widget-list">
            <?php dynamic_sidebar('molly'); ?>
        </ul>
    </li>
</ul>
<style type="text/css">
#right-column { margin-top: 750px; }
#right-column h4 
{
    margin-top: 2em;
    font-size: 1.5em;
    font-weight: bold; 
}
#right-column iframe#twitter-widget-0
{
    width: 330px!important;
}
</style>
<?php dynamic_sidebar('molly-footer'); ?>
<?php get_footer(); ?>
