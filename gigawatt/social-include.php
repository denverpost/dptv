<?php
if ( $https == '' && is_ssl() == False ):
    $text = html_entity_decode(get_the_title());
    //$link = get_permalink();
    $link = wp_get_shortlink();
    if ( (is_single() || is_page() ) && has_post_thumbnail($post->ID) ) {
        $image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large');
    } else {
        $image = get_stylesheet_directory_uri() . '/images/logo-large.png';
    }
    $stylesheet_uri = get_stylesheet_directory_uri();

    $desc = ( get_the_excerpt() != '' ? get_the_excerpt() : get_bloginfo('description') );
    $social_string = '<ul class="post-meta-social-links" id="sticky-inner">';
    // Embed button
    $social_string .= sprintf(
        '<li class="post-meta-social pm-embed"><a href="javascript:void(0)" onclick="jQuery(\'#embed-markup\').toggleClass(\'hide\'); return false;"><img src="%1$s" alt="Embed this video" /></a></li>',
        $stylesheet_uri . '/images/dptv-embed.png'
    );
    //Twitter button
    $social_string .= sprintf(
        '<li class="post-meta-social pm-twitter"><a href="javascript:void(0)" onclick="javascript:window.open(\'http://twitter.com/share?text=%1$s&url=%2$s&via=%4$s&related=MollyHughes\', \'twitwin\', \'left=20,top=20,width=500,height=500,toolbar=1,resizable=1\');"><img src="%3$s" alt="Tweet this story" /></a></li>',
        urlencode(html_entity_decode($text, ENT_COMPAT, 'UTF-8') . ':'),
        rawurlencode( $link ),
        $stylesheet_uri . '/images/dptv-twitter.png',
        'DPTV'
    );
    //Facebook share
    $social_string .= sprintf(
        '<li class="post-meta-social pm-facebook"><a href="javascript:void(0)" onclick="javascript:window.open(\'http://www.facebook.com/sharer/sharer.php?s=100&p[url]=%1$s&p[images][0]=%2$s&p[title]=%3$s&p[summary]=%4$s\', \'fbwin\', \'left=20,top=20,width=500,height=500,toolbar=1,resizable=1\');"><img src="%5$s" alt="Facebook" /></a></li>',
        rawurlencode( $link ),
        rawurlencode( $image[0] ),
        urlencode( html_entity_decode($text, ENT_COMPAT, 'UTF-8') ),
        urlencode( $desc ),
        $stylesheet_uri . '/images/dptv-facebook.png'
    );
    $social_string .= '<div class="clear"></div></ul></div>';
    echo $social_string;
endif;  // Close the https check. We don't want social share on https.
?>
