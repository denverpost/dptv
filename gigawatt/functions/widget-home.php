<?php dynamic_sidebar('slider');
if (function_exists('dynamic_sidebar') && is_active_sidebar('homepage')) : ?> 
	<div class="home-sidebar">	
		<div id="widget-block" class="clearfix">
			<ul class="widget-list">
				<?php dynamic_sidebar('homepage'); ?>
<li class="content-widget post-content-widget widget clearfix">
			<ul class="three-column content-widget-item post clearfix">
				<?php dynamic_sidebar('homepage-middle'); ?>
            </ul>
</li>
				<?php dynamic_sidebar('homepage-bottom'); ?>
			</ul>
		</div>
	</div>	
<?php endif; ?>
