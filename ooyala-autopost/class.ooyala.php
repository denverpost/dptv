<?php

class Ooyala
{

    var $json;
    var $url;
    var $test;
    var $path_prefix;

    function __construct()
    {
        global $wpdb;
        $this->url = '';
        $this->test = true;
        $this->path_prefix = '';
        $this->label_id = '';
        $this->api = '';
        $this->api_keys = Array(
            'public' => '',
            'private' => '');
        if ( function_exists('plugin_dir_path') ):
            $this->path_prefix = plugin_dir_path( __FILE__ );
        endif;

        if ( !class_exists('Easy_CF') ):
            require_once( WP_PLUGIN_DIR . '/easy-custom-fields/easy-custom-fields.php' );
        endif;
        //if ( class_exists('Easy_CF') ):
            $test = $wpdb->get_results( "SELECT meta_key FROM wp_postmeta where meta_key='video_id'" );
            if ( $wpdb->num_rows == 0 ):
                // Create the custom fields
                $field_data = array (
                    'Ooyala' => array (             // unique group id
                        'fields' => array(             // array "fields" with field definitions
                            'video_id'  => array(),      // globally unique field id
                            'preview_image_url'  => array(),
                            'duration'  => array()
                        ),
                    ),
                );
                $easy_cf = new Easy_CF($field_data);
            endif;
        //endif;
    }

    public function set_test($test)
    {
        // Set the value of the test var.
        return $this->test = $test;
    }

    public function setup_api()
    {
        // Connect to Ooyala's API.
        // Assumes API key values have already been set.
        if ( $this->api_keys['public'] == '' ):
            // ^^^ Raise error, exit
            return false;
        endif;
        if ( $this->api_keys['private'] == '' ):
            // ^^^ Raise error, exit
            return false;
        endif;

        $this->api = new OoyalaInterface($this->api_keys['public'], $this->api_keys['private']);
    }

    public function get_json()
    {
        // Get the JSON
        if ( $this->test == true ):
            $this->url = $this->path_prefix . 'updates.json';
        endif;

        $this->json = file_get_contents($this->url);
        return $this->json;
    }

    public function write_json()
    {
        if ( $this->json === false ):
            $this->json = $this->get_json();
        endif;
        if ( $this->json != false ):
            return file_put_contents($this->path_prefix . 'updates.json', $this->json);
        endif;
        return false;
    }

    public function parse_json()
    {
        // Take the json in updates.json and pull out the chunk that we want.
        // Will have to build this method out if we want to measure things
        // in addition to the Rockies wins.
        if ( $this->json === false ):
            $this->json = $this->get_json();
        endif;
        $object = json_decode($this->json);

        return $data;
    }

}
