<?php

include('lib/OoyalaAPI.php');

/**
 * This class allows for easy interfacing with Ooyala's Service.
 *
 * @author Tim Kerstiens
 */
class OoyalaInterface extends OoyalaAPI {
	/**
	 * Holds a message for when an asset is not ready to be replaced, generally because it is currently being replaced
	 */
	const ASSET_NOT_READY_MESSAGE = 'The asset is already being replaced';

	/**
	 * Constants used to help decide which video stream to get
	 */
	const MAX_BITRATE = 0;
	const MIN_BITRATE = -1;
	
	/**
	 * Sole constructor.  Takes the Ooyala public/private key pair to initilize the interface.  Set your default keys here if desired.
	 *
	 * @param String publicKey The public key used to interface with Ooyala
	 * @param String privateKey The private key used to interface with Ooyala
	 */
	public function __construct($publicKey = '', $privateKey = '') {
		parent::__construct($publicKey, $privateKey);
	}

	/**
	 * Retrieves the next page of results for GET requests.  Will return false if there is not a next page, making it useful for do-while loops
	 *
	 * @param String $nextPageURL The URL of the next page
	 * 
	 * @throws OoyalaRequestErrorException
	 *
	 * @return Mixed Will return an Ooyala Response (json_decoded) object with the next page of results, if there was a next page.  If there is no
	 *     next page this method will return false
	 */
	public function getNextPage($ooyalaResponseObject) {
		if(property_exists($ooyalaResponseObject, 'next_page')) {
			$URLParts = parse_url($ooyalaResponseObject->next_page);
			$queryParameters = array();
			parse_str($URLParts['query'], $queryParameters);
			return $this->get($URLParts['path'], $queryParameters);
		} else {
			return false;
		}
	}

	/**
	 * Gets a stream URL based on the parameters passed into the function.  If nothing matches then the source video will be returned.
	 *     PLEASE NOTE: Source video might not be web safe!
	 *
	 * @param String $embedCode The embed code (video ID) to retrieve
	 * @param Int $desiredBitRate The desired bit rate that you would like to retrieve. Options include
	 *     OoyalaInterface::MAX_BITRATE (0)
	 *     OoyalaInterface::MIN_BITRATE (-1)
	 *     Any interger greater than or equal to 1, that represents the average bit rate you would like.  This will pull the stream that most closely
	 *         matches this number.
	 * @param Array $acceptableEncodingTypes The acceptable encoding types to pull.  Defaults to pulling only h264 encoded videos.
	 * @param Array $acceptableMuxingTypes The acceptable muxing types to pull.  Defaults to pulling only MP4 videos.
	 * 
	 * @throws OoyalaRequestErrorException
	 *
	 * @return String This method will return a URL to a video resource.
	 */
	public function getStreamURL($embedCode, $desiredBitRate = OoyalaInterface::MAX_BITRATE, $acceptableEncodingTypes = array('h264'), 
		$acceptableMuxingTypes = array('MP4')) {
		if(!is_array($acceptableEncodingTypes)) {
			throw new OoyalaRequestErrorException('Acceptable encoding types must be in an array format');
		}
		if(!is_array($acceptableMuxingTypes)) {
			throw new OoyalaRequestErrorException('Acceptable muxing types must be in an array format');
		}
		if(!is_numeric($desiredBitRate)) {
			throw new OoyalaRequestErrorException('Desired bit rate must be a number');
		}
		
		$streams = $this->get('assets/' . $embedCode . '/streams');
		$numberOfStreams = count($streams);
		$sourceStream = $streams[0];
		
		for($i = 0; $i < $numberOfStreams; $i++) {
			if(!in_array($streams[$i]->video_codec, $acceptableEncodingTypes)) {
				unset($streams[$i]);
				continue;
			}
			if(!in_array($streams[$i]->muxing_format, $acceptableMuxingTypes)) {
				unset($streams[$i]);
				continue;
			}
		}

		$sortFunction = function($a, $b) use ($desiredBitRate) {
			if($a->average_video_bitrate == $b->average_video_bitrate) {
				return 0;
			}
			
			if($desiredBitRate == OoyalaInterface::MAX_BITRATE) {
				return ($a->average_video_bitrate > $b->average_video_bitrate) ? -1 : 1;
			} else if($desiredBitRate == OoyalaInterface::MIN_BITRATE) {
				return ($a->average_video_bitrate > $b->average_video_bitrate) ? 1 : -1;
			} else {
				$distanceA = abs($desiredBitRate - $a->average_video_bitrate);
				$distanceB = abs($desiredBitRate - $b->average_video_bitrate);

				if($distanceA == $distanceB) {
					return 0;
				}
				
				return ($distanceA > $distanceB) ? 1 : -1;
			}
		};
		
		usort($streams, $sortFunction);
		
		if(empty($streams)) {
			return $sourceStream->url;
		} else {
			return $streams[0]->url;
		}
	}

	/**
	 * This method will pull the preview image for the asset.  It really is only a convienance method, nothing special is done
	 *
	 * @param String $embedCode The asset to pull the preview image for
	 *
	 * @throws OoyalaRequestErrorException
	 *
	 * @return String Returns the URL to the assets preview image
	 */
	public function getPreviewImageURL($embedCode) {
		return $this->get('assets/' . $embedCode)->preview_image_url;
	}

	/**
	 * This method makes translating a label's full path into a label id easy
	 *
	 * @param String $labelFullPath The Ooyala full path to the label you want the ID for. Do NOT include the initial /
	 * 
	 * @throws OoyalaRequestErrorException Can be used to check that the label was not found, as well as other errors.
	 *
	 * @return String Returns a String containing the label ID
	 */
	public function getLabelIDByFullPath($labelFullPath) {
		// Make sure the user did not pass in the initial /
		if(strpos($labelFullPath, '/') === 0) {
			$labelFullPath = str_replace('/'. ''. $labelFullPath, 1);
		}
		// %2F is a urlencoded /.  For some reason PHP cannot generate the correct signature if the first / of the full path is not encoded
		return $this->get('labels/by_full_path/%2F' . $labelFullPath)->items[0]->id;
	}
	
	/**
	 * This method will upload a video asset to the Ooyala service and return the embed code (aka asset ID or video ID)
	 *
	 * @param Stream $videoFileHandle A file handle to the file that contains the video to be uploaded
	 * @param Int $videoFileSize The size of the file to be uploaded
	 * @param String $assetTitle The title to attach to the asset after it is uploaded
	 * @param String $fileName The name of the file being uploaded.  Optional, defaults to assetTitle
	 * @param Array $additionalParameters An array of additional request parameters for the initial POST of the asset.  For things such as description
	 *     or external_id.  Anything not REQUIRED for the initial POST but is still wanted should be put in this array
	 *
	 * @throws OoyalaRequestErrorException
	 *
	 * @return String This method returns the embed code for the newly uploaded asset
	 */
	public function uploadVideo($videoFileHandle, $videoFileSize, $assetTitle, $fileName = null, $additionalParameters = array()) {
		if(!is_resource($videoFileHandle) || get_resource_type($videoFileHandle) != 'stream') {
			throw new OoyalaRequestErrorException('Unusable file handle');
		}
		if($fileName == null) {
			$fileName = $assetTitle;
		}
		
		$requestBody = array(
			'name' => $assetTitle,
			'file_name' => $fileName,
			'asset_type' => 'video',
			'file_size' => $videoFileSize
		); $requestBody += $additionalParameters;
		
		// Create the asset on Ooyala
		$response = $this->post('assets', $requestBody); // Throws OoyalaRequestErrorException
		$embedCode = $response->embed_code;
		
		// Get the URL to upload the video to
		$response = $this->get('assets/' . $embedCode . '/uploading_urls');
		$uploadURL = $response[0];
		
		// Setup CURL request to upload video
		$CH = curl_init();
		curl_setopt($CH, CURLOPT_URL, $uploadURL);
		curl_setopt($CH, CURLOPT_PUT, 1);
		curl_setopt($CH, CURLOPT_INFILE, $videoFileHandle);
		curl_setopt($CH, CURLOPT_INFILESIZE, $videoFileSize);
		curl_setopt($CH, CURLOPT_RETURNTRANSFER, 1);
		
		// Upload the video
		$CURLResponse = curl_exec($CH);
		$headers = curl_getinfo($CH);
		$responseCode = $headers['http_code'];
		
		curl_close($CH);
		
		if($responseCode < 400) {
			$this->put('assets/' . $embedCode . '/upload_status', array('status' => 'uploaded'));
		} else {
			throw new OoyalaRequestErrorException($CURLResponse);
		}
		
		return $embedCode;
	}
	
	/**
	 * This method will replace a video on an existing asset hosted by the Ooyala service by using the embed code (aka asset or video ID) or an 
	 *     external ID.
	 *
	 * @param String $assetID This is the asset ID of the asset that you want to replace the video
	 * @param Stream $videoFileHandle This should be a file handle to the video that needs to be uploaded
	 * @param Int The filesize of the file being uploaded
	 * @param String $assetTitle The title of the asset on Ooyala.  Does not need to be different from the original but it is still required
	 * @param String $fileName The name of the file being uploaded.  Optional.
	 *
	 * @throws OoyalaRequestErrorException Thrown when there is an error with a request being made
	 * @throws OoyalaAssetNotReadyException Thrown when an asset is not ready to be replaced
	 *
	 * @return void
	 */
	public function replaceVideo($assetID, $videoFileHandle, $videoFileSize, $assetTitle, $fileName = null) {
		if(!is_resource($videoFileHandle) || get_resource_type($videoFileHandle) != 'stream') {
			throw new OoyalaRequestErrorException('Unusable file handle');
		}
		if($fileName == null) {
			$fileName = $assetTitle;
		}
		
		$requestBody = array(
			'name' => $assetTitle,
			'file_name' => $fileName,
			'asset_type' => 'video',
			'file_size' => $videoFileSize
		);
		
		try {
			$this->post('assets/' . $assetID . '/replacement', $requestBody);
		} catch(OoyalaRequestErrorException $e) {
			if(OoyalaInterface::parseErrorMessage($e) == OoyalaInterface::ASSET_NOT_READY_MESSAGE) {
				throw new OoyalaAssetNotReadyException(OoyalaInterface::ASSET_NOT_READY_MESSAGE);
			} else {
				throw new OoyalaRequestErrorException($e->getMessage());
			}
		}
		$response = $this->get('assets/' . $assetID . '/replacement/uploading_urls');
		
		$uploadURL = $response[0];
		
		$CH = curl_init();
		curl_setopt($CH, CURLOPT_URL, $uploadURL);
		curl_setopt($CH, CURLOPT_PUT, 1);
		curl_setopt($CH, CURLOPT_INFILE, $videoFileHandle);
		curl_setopt($CH, CURLOPT_INFILESIZE, $videoFileSize);
		curl_setopt($CH, CURLOPT_RETURNTRANSFER, 1);
		
		$CURLResponse = curl_exec($CH);
		$headers = curl_getinfo($CH);
		$responseCode = $headers['http_code'];
		curl_close($CH);
		
		if($responseCode < 400) {
			$this->put('assets/' . $assetID . '/replacement/upload_status', array('status' => 'uploaded'));
		} else {
			throw new OoyalaRequestErrorException($CURLResponse);
		}
	}
	
	/**
	 * This method attempts to pull the useful part of the message out of the mix of formats that the OoyalaAPI class puts in it's exception messages
	 *
	 * @param mixed error The exception thrown by the OoyalaAPI and OoyalaInterface classes or a string; like from a CURL request, for example
	 * 
	 * @return String This method returns a useful message from the exception or the exception message itself if it cannot be parsed correctly.
	 */
	public static function parseErrorMessage($error) {
		$message = (is_string($error)) ? $error : $error->getMessage();
		// Look for the 503 Service Unavailable that sometimes pops up
		if(stripos($error->getMessage(), 'Error 503') !== false) {
			return '503: Service Unavailable';
		}
		// Look for current asset in upload stage
		if(stripos($error->getMessage(), 'being replaced') !== false) {
			return OoyalaInterface::ASSET_NOT_READY_MESSAGE;
		}
		// Looking for any JSON encoded part
		$matches = array();
		preg_match('~{.*}~', $error->getMessage(), $matches);
		// If there is no JSON, we don't know what to look for so just return getMessage
		if(empty($matches)) {
			return $error->getMessage();
		}
		$response = json_decode($matches[0]);
		// If the JSON has the standard message object in it, return that, otherwise we don't know where to find the message so just return getMessage
		if(property_exists($response, 'message') && is_string($response->message)) {
			return $response->message;
		} else {
			return $error->getMessage();
		}
	}
	
	/**
	 * The main method is used for having the class test itself and can also be read through to see how to utilize the class.
	 *
	 * @param array $arguments The arguments array passed into php
	 */
	public static function main($arguments) {
		error_reporting(0);
		
		require('lib/TestResults/TestResults.php');
		$results = array();
		
		// First, test a bad API key pair and check the output of the parseErrorMessage method
		$ooyalaInterface = new OoyalaInterface('thisisabadkey');
		try {
			$ooyalaInterface->get('assets');
		} catch(OoyalaRequestErrorException $e) {
			if(!is_string(OoyalaInterface::parseErrorMessage($e))) {
				$results['parseErrorMessage'] = false;
			} else {
				$results['parseErrorMessage'] = true;
			}
		}
		
		// Now make sure that our construct functions are working properly
		$ooyalaInterface = new OoyalaInterface();
		try {
			$response = $ooyalaInterface->get('assets', array('limit' => '10')); // Throws OoyalaRequestErrorException
			// Will not make it here if something went wrong.
			$results['__construct'] = true;
		} catch(OoyalaRequestErrorException $e) {
			echo '__construct: Error - ' . OoyalaInterface::parseErrorMessage($e) . PHP_EOL;
			$results['__construct'] = false;
		}
		
		// Next, check the uploadVideo method
		// First, with a string instead of a file handle resource
		try {
			$response = $ooyalaInterface->uploadVideo('notevenaresouce', 0, 'TestVideo'); // Should throw an OoyalaRequestErrorException
			$results['uploadVideo'] = false;
		} catch(OoyalaRequestErrorException $e) {
			try {
				// Second, with a failed fopen.  Throws PHP Warnings
				$fileHandle = fopen('doesntEvenExist.mov', 'r');
				$response = $ooyalaInterface->uploadVideo($fileHandle, 0, 'TestVideo'); // Should throw an OoyalaRequestErrorException
				$results['uploadVideo'] = false;
			} catch(OoyalaRequestErrorException $e) {
				// Lastly, try with a valid file handle.  This should be a file pointed at by $arguments[1]
				try {
					if(!isset($arguments[1]) || !file_exists($arguments[1])) {
						$results['uploadVideo'] = 'Unable to complete due to no file provided';
					} else {
						$fileHandle = fopen($arguments[1], 'r');
						$fileSize = filesize($arguments[1]);
						$response = $ooyalaInterface->uploadVideo($fileHandle, $fileSize, 'TestVideo', 'TestVideo.mov', 
							array('external_id' => 'DELETEMENOW'));
						if(is_string($response)) {
							$results['uploadVideo'] = true;
							// Test the replaceVideo method next.  Test with a bad asset ID
							try {
								// Test a bad asset ID
								$ooyalaInterface->replaceVideo('DELETE', $fileHandle, $fileSize, 'Test Video 2: The Legend Continues', 
									'TestVideo2.mov');
								$results['replaceVideo'] = false;
							} catch(OoyalaRequestErrorException $e) {
								// Cannot test without a different file
								if(!isset($arguments[2]) || !file_exists($arguments[2])) {
									$results['replaceVideo'] = 'Unable to complete due to no file provided';
								} else {
									$fileHandle = fopen($arguments[2], 'r');
									$fileSize = filesize($arguments[2]);
									try {
										// Test the replace, but give the original upload time to process, but still output so we know we aint stuck
										$count = 1;
										while($count <= 12) {
											sleep(5);
											echo 'Just slept 5 seconds, total of ' . ($count * 5) . PHP_EOL;
											$count++;
										}
										$ooyalaInterface->replaceVideo('DELETEMENOW', $fileHandle, $fileSize, 'Test Video 2: The Legend Continues', 
											'TestVideo2.mov');
										$results['replaceVideo'] = true;
										try {
											// Test the OoyalaAssetNotReadyException
											$ooyalaInterface->replaceVideo('DELETEMENOW', $fileHandle, $fileSize, 
												'Test Video 2: The Legend Continues', 'TestVideo2.mov');
											$results['OoyalaAssetNotReadyException'] = false;
										} catch(OoyalaRequestErrorException $e) {
											$results['OoyalaAssetNotReadyException'] = false;
											echo 'OoyalaAssetNotReadyException Exception Error - ' . OoyalaInterface::parseErrorMessage($e) . PHP_EOL;
										} catch(OoyalaAssetNotReadyException $e) {
											$results['OoyalaAssetNotReadyException'] = true;
										}
									} catch(OoyalaRequestErrorException $e) {
										$results['replaceVideo'] = false;
										echo 'replaceVideo method error - ' . OoyalaInterface::parseErrorMessage($e) . PHP_EOL;
									} catch(OoyalaAssetNotReadyException $e) {
										$results['replaceVideo'] = false;
										echo 'replaceVideo method error - ' . OoyalaInterface::parseErrorMessage($e) . PHP_EOL;
									}
								}
							} catch (OoyalaAssetNotReadyException $e) {
								$results['replaceVideo'] = false;
								echo 'Something went wrong with replaceVideo method' . OoyalaInterface::parseErrorMessage($e) . PHP_EOL;
							}
							if(!isset($arguments[3]) || $arguments[3] != 'save') {
								$ooyalaInterface->delete('assets/' . $response);
							}
						} else {
							$results['uploadVideo'] = false;
							echo '$response = ' . var_dump($response) . PHP_EOL;
						}
					}
				} catch(OoyalaRequestErrorException $e) {
					$results['uploadVideo'] = false;
					echo OoyalaInterface::parseErrorMessage($e) . PHP_EOL;
				}
			}
		}
		// Testing the getStreamURL method
		// You will need to provide an embed code to test these with properly.
		$embedCode = '';
		try {
			$results['getStreamURL'] = false;
			if(filter_var($ooyalaInterface->getStreamURL($embedCode), FILTER_VALIDATE_URL)) {
				$results['getStreamURL'] = true;
			}

			$results['getPreviewImageURL'] = false;
			if(filter_var($ooyalaInterface->getPreviewImageURL($emedCode), FILTER_VALIDATE_URL)) {
				$results['getPreviewImageURL'] = true;
			}
		} catch(OoyalaRequestErrorException $e) {
			echo 'Embed code may need to be updated...' . PHP_EOL;
			$results['getStreamURL'] = OoyalaInterface::parseErrorMessage($e);
			$results['getPreviewImageURL'] = $results['getStreamURL'];
		}

		// Testing getLabelIDByFullPath method
		// You will need to provide a full path to test these with properly.  Make sure not to use the initial slash (/)!
		$labelFullPath = '';
		$results['getLabelIDByFullPath'] = false;
		try {
			$labelID = $ooyalaInterface->getLabelIDByFullPath($labelFullPath);
			if(is_string($labelID) && !empty($labelID)) {
				$results['getLabelIDByFullPath'] = true;
				// Testing getNextPage method
				// You will need to provide a label that has more than one video in it using the $labelFullPath variable above.
				$results['getNextPage'] = false;
				try {
					$page = 0;
					$ooyalaResponse = $ooyalaInterface->get('labels/' . $labelID . '/assets', array('limit' => 1));
					do {
						if($page > 0) { // If we can make it to the second page we should be golden, or an exception will be thrown
							$results['getNextPage'] = true;
						} $page++;
					} while($ooyalaResponse = $ooyalaInterface->getNextPage($ooyalaResponse));
				} catch(OoyalaRequestErrorException $e) {
					$results['getNextPage'] = OoyalaInterface::parseErrorMessage($e);
				}
			}
		} catch(OoyalaRequestErrorException $e) {
			echo 'Label full path may need to be updated...' . PHP_EOL;
			$results['getLabelIDByFullPath'] = OoyalaInterface::parseErrorMessage($e);
		}

		$results = new TestResults($results);
		$results->display();
		$results->displaySummary();
	}	
}

/**
 * Thrown when a video is trying to be replaced but cannot be replaced at this time
 */
class OoyalaAssetNotReadyException extends Exception {}

if(isset($_SERVER['SCRIPT_FILENAME']) && $_SERVER['SCRIPT_FILENAME'] == 'OoyalaInterface.php') {
	OoyalaInterface::main($argv);
}
