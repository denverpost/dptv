<?php
include('simplepie_1.3.1.compiled.php');

class Inform
{

    var $json;
    var $url;
    var $test;
    var $path_prefix;

    function __construct()
    {
        global $wpdb;
        $this->url = '';
        $this->test = true;
        $this->path_prefix = '';
        $this->label_id = '';
        $this->api = '';
        $this->api_keys = Array(
            'public' => '',
            'private' => '');
        if ( function_exists('plugin_dir_path') ):
            $this->path_prefix = plugin_dir_path( __FILE__ );
        endif;

        if ( !class_exists('Easy_CF') ):
            require_once( WP_PLUGIN_DIR . '/easy-custom-fields/easy-custom-fields.php' );
        endif;
        //if ( class_exists('Easy_CF') ):
            $test = $wpdb->get_results( "SELECT meta_key FROM wp_postmeta where meta_key='video_id'" );
            if ( $wpdb->num_rows == 0 ):
                // Create the custom fields
                $field_data = array (
                    'Inform' => array (             // unique group id
                        'fields' => array(             // array "fields" with field definitions
                            'video_id'  => array(),      // globally unique field id
                            'preview_image_url'  => array(),
                            'duration'  => array()
                        ),
                    ),
                );
                $easy_cf = new Easy_CF($field_data);
            endif;
        //endif;
    }

    public function set_test($test)
    {
        // Set the value of the test var.
        return $this->test = $test;
    }

    public function get_xml($url, $limit=15)
    {
        // Get the XML
        if ( $this->test == true ):
            $this->url = $this->path_prefix . 'updates.xml';
        endif;

        $feed = new SimplePie();
        $feed->set_feed_url($url);
        //$feed->enable_cache($this->enable_cache);
        $feed->set_timeout(15);
        //$feed->set_cache_duration($this->input['cache_duration']);
        $feed->set_useragent('DenverPost Scraper/0.1');
        $feed->set_output_encoding('UTF-8');
        //$feed->xml_dump(true);
        $feed->set_item_limit($this->input['items_per_feed']);
        $feed->init();
        $feed->handle_content_type();

        return $feed;
    }

    /*
    public function write_xml()
    {
        if ( $this->xml === false ):
            $this->xml = $this->get_xml();
        endif;
        if ( $this->xml != false ):
            return file_put_contents($this->path_prefix . 'updates.xml', $this->xml);
        endif;
        return false;
    }
    */

    public function parse_xml()
    {
        // Take the xml in updates.xml and pull out the chunk that we want.
        // Relies on SimplePie RSS library.
        if ( $this->xml === false ):
            $this->xml = $this->get_xml();
        endif;
        $object = xml_decode($this->xml);

        return $data;
    }

    public function get_playlists()
    {
        // Return a list of current playlists.
    }
}

class SimplePieInform extends SimplePie
{
}
/* An Inform feed item looks something like this:
    <item>
      <guid isPermaLink="false">http://playlists.newsinc.com/feeds/playlist/12608/29796513</guid>
      <link>http://widget.newsinc.com/single.htm?vid=29796513&amp;cid=12608&amp;freewheel=90115&amp;sitesection=ndn</link>
      <title>DPTV_ROBIN_SPORTS_MINUTE_100915</title>
      <description>DPTV_ROBIN_SPORTS_MINUTE_100915</description>
      <pubDate>10/9/2015</pubDate>
      <duration>155.03</duration>
      <link href="http://widget.newsinc.com/single.htm?vid=29796513&amp;cid=12608&amp;freewheel=90115&amp;sitesection=ndn" xmlns="http://www.w3org/2005/Atom" />
      <updated xmlns="http://www.w3.org/2005/Atom">10/9/2015 12:41:00 PM</updated>
      <image width="320" height="240"> http://thumbnail.newsinc.com/29796513.jpg</image>
      <owner xmlns="http://base.google.com/cns/1.0">Denver Post </owner>
      <VideoId xmlns="http://base.google.com/cns/1.0">29796513</VideoId>
      <singleembed xmlns="http://base.google.com/cns/1.0"><![CDATA[<iframe src="http://widget.newsinc.com/single.htm?vid=29796513&cid=12608&freewheel=90115&sitesection=ndn&wid=2" height="300" width="425" frameborder=no scrolling=no noresize marginwidth=0px marginheight=0px></iframe>]]></singleembed>
      <dynamicEmbed1 xmlns="http://base.google.com/cns/1.0"><![CDATA[<div id="ndn_single_embed_1"><script type="text/javascript" src="http://embed.newsinc.com/single/embed.js?vid=29796513&freewheel=90115&sitesection=ndn&wid=2&height=360&width=600&parent=ndn_single_embed_1"></script></div>]]></dynamicEmbed1>
      <dynamicEmbed2 xmlns="http://base.google.com/cns/1.0"><![CDATA[<iframe src="http://embed.newsinc.com/single/iframe.html?vid=29796513&freewheel=90115&sitesection=ndn&wid=2&height=360&width=600" height="360" width="600" parent=ndn_single_embed_1 frameborder=no scrolling=no noresize marginwidth=0px marginheight=0px></iframe>]]></dynamicEmbed2>
      <jsembednew xmlns="http://base.google.com/cns/1.0"><![CDATA[<div class="ndn_embed" style="height:360px;width:600px;" data-config-widget-id="2" data-config-type="VideoPlayer/Single" data-config-tracking-group="90115" data-config-playlist-id="12608" data-config-video-id="29796513" data-config-site-section="ndn"></div>]]></jsembednew>
      <media:content url="http://thumbnail.newsinc.com/29796513.sf.jpg" width="320" height="240" lang="en"></media:content>
      <landingPageURL xmlns="http://base.google.com/cns/1.0">http://www.denverpost.com/newsvideo?vcid=29796513&amp;freewheel=90115&amp;sitesection=ndn</landingPageURL>
    </item>
*/
